package id.co.indivara.jdt12.miniproject;

import id.co.indivara.jdt12.miniproject.controller.AuthenticationController;
import id.co.indivara.jdt12.miniproject.dto.request.AuthenticateRequest;
import id.co.indivara.jdt12.miniproject.dto.response.AuthenticationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertNotNull;
public class LoginData {
      public static final String adminUsername= "admin";
      public static final String adminPassword="admin";
      public static final String customerUsername="customer";
      public static final String customerPassword="customer";
      public static final String driverUsername="driver0";
      public static final String driverPassword="driver0";

}
