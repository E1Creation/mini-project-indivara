package id.co.indivara.jdt12.miniproject.admin;

import id.co.indivara.jdt12.miniproject.LoginData;
import id.co.indivara.jdt12.miniproject.controller.AuthenticationController;
import id.co.indivara.jdt12.miniproject.controller.DriverController;
import id.co.indivara.jdt12.miniproject.dto.request.AuthenticateRequest;
import id.co.indivara.jdt12.miniproject.dto.response.AuthenticationResponse;
import id.co.indivara.jdt12.miniproject.entity.Driver;
import id.co.indivara.jdt12.miniproject.service.DriverService;
import id.co.indivara.jdt12.miniproject.utilize.mapper.MapperConvertion;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.Objects;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DriverControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private AuthenticationController authenticationController;
    private static ResponseEntity<AuthenticationResponse> response;
    @Autowired
    private DriverController driverController;
    @Autowired
    private DriverService driverService;

    @Before
    public void before() throws Exception {
        AuthenticateRequest authenticateRequest = new AuthenticateRequest(LoginData.adminUsername,LoginData.adminPassword);
        response = authenticationController.login(authenticateRequest);
        System.out.println(Objects.requireNonNull(response.getBody()).getAccessToken());
    }

    @Test
    public void unAuthorizeGetAllDriverTest() throws Exception {
        List<Driver> driver = driverService.getAll();
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/driver")
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isForbidden());


    }
    @Test
    public void getAllDriverTest() throws Exception {
        List<Driver> driver = driverService.getAll();
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/driver")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization","Bearer "+ Objects.requireNonNull(response.getBody()).getAccessToken())
                )
                .andDo(result -> {
                    List<Driver> drivers = MapperConvertion.getAllData(result.getResponse().getContentAsString(), Driver.class);
                    Assertions.assertNotNull(drivers);
                    Assertions.assertEquals(driver.get(0).getName(), drivers.get(0).getName());
                })
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNotEmpty());

    }
    @Test
    public void getDriverById() throws Exception {
        Driver driver = driverService.findById(1L);
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/driver/1")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization","Bearer "+ Objects.requireNonNull(response.getBody()).getAccessToken())
                )
                .andDo(result -> {
                    Driver drivers = MapperConvertion.getData(result.getResponse().getContentAsString(), Driver.class);
                    Assertions.assertNotNull(drivers);
                    Assertions.assertEquals(driver.getName(),drivers.getName());
                })
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty());
    }
    @Test
    public void unAuthorizeGetDriverById() throws Exception {
        Driver driver = driverService.findById(1L);
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/driver/1")
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isForbidden());

    }
    @Test
    public void updateDriver() throws Exception {
        Driver driver = driverService.findById(1L);
        mockMvc.perform(MockMvcRequestBuilders
                        .put("/api/driver/1")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertion.toJson(driver))
                        .header("Authorization","Bearer "+ Objects.requireNonNull(response.getBody()).getAccessToken())
                )
                .andDo(result -> {
                    Driver drivers = MapperConvertion.getData(result.getResponse().getContentAsString(), Driver.class);
                    Assertions.assertNotNull(drivers);
                    Assertions.assertEquals(drivers.getName(),driver.getName());
                })
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty());
    }
    @Test
    public void unAuthorizeUpdateDriver() throws Exception {
        Driver driver = driverService.findById(1L);
        mockMvc.perform(MockMvcRequestBuilders
                        .put("/api/driver/1")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertion.toJson(driver))
                )
                .andExpect(status().isForbidden());
    }
}
