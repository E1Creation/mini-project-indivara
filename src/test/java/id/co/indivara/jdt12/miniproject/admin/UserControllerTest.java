package id.co.indivara.jdt12.miniproject.admin;

import id.co.indivara.jdt12.miniproject.LoginData;
import id.co.indivara.jdt12.miniproject.controller.AuthenticationController;
import id.co.indivara.jdt12.miniproject.controller.UserController;
import id.co.indivara.jdt12.miniproject.dto.request.AuthenticateRequest;
import id.co.indivara.jdt12.miniproject.dto.response.AuthenticationResponse;
import id.co.indivara.jdt12.miniproject.entity.User;
import id.co.indivara.jdt12.miniproject.service.UserService;
import id.co.indivara.jdt12.miniproject.utilize.mapper.MapperConvertion;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.Objects;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private AuthenticationController authenticationController;
    private static ResponseEntity<AuthenticationResponse> response;
    @Autowired
    private UserController userController;
    @Autowired
    private UserService userService;

    @Before
    public void before() throws Exception {
        AuthenticateRequest authenticateRequest = new AuthenticateRequest(LoginData.adminUsername,LoginData.adminPassword);
        response = authenticationController.login(authenticateRequest);
        System.out.println(Objects.requireNonNull(response.getBody()).getAccessToken());
    }

    @Test
    public void getAllUserTest() throws Exception {
        List<User> userList = userService.getAll();
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/user")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization","Bearer "+ Objects.requireNonNull(response.getBody()).getAccessToken())
                )
                .andDo(result -> {
                    List<User> users = MapperConvertion.getAllData(result.getResponse().getContentAsString(), User.class);
                    Assertions.assertNotNull(users);
                    Assertions.assertEquals(userList.get(0).getUsername(), users.get(0).getUsername());
                })
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNotEmpty());

    }
    @Test
    public void unAuthorizeGetAllUserTest() throws Exception {
        List<User> userList = userService.getAll();
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/user")
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isForbidden());
    }
    @Test
    public void getUserById() throws Exception {
        User user = userService.findById(1L);
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/user/1")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization","Bearer "+ Objects.requireNonNull(response.getBody()).getAccessToken())
                )
                .andDo(result -> {
                    User users = MapperConvertion.getData(result.getResponse().getContentAsString(), User.class);
                    Assertions.assertNotNull(users);
                    Assertions.assertEquals(user.getUsername(),users.getUsername());
                })
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty());
    }
    @Test
    public void unAuthorizeGetUserById() throws Exception {
        User user = userService.findById(1L);
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/user/1")
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isForbidden());
    }
    @Test
    public void updateUser() throws Exception {
        User user = userService.findById(1L);
        mockMvc.perform(MockMvcRequestBuilders
                        .put("/api/user/1")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertion.toJson(user))
                        .header("Authorization","Bearer "+ Objects.requireNonNull(response.getBody()).getAccessToken())
                )
                .andDo(result -> {
                    User users = MapperConvertion.getData(result.getResponse().getContentAsString(), User.class);
                    Assertions.assertNotNull(users);
                    Assertions.assertEquals(users.getUsername(),user.getUsername());
                })
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty());
    }
    @Test
    public void unAuthorizeUpdateUser() throws Exception {
        User user = userService.findById(1L);
        mockMvc.perform(MockMvcRequestBuilders
                        .put("/api/user/1")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertion.toJson(user))
                )
                .andExpect(status().isForbidden());

    }

}
