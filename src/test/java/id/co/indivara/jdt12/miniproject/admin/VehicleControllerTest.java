package id.co.indivara.jdt12.miniproject.admin;

import id.co.indivara.jdt12.miniproject.LoginData;
import id.co.indivara.jdt12.miniproject.controller.AuthenticationController;
import id.co.indivara.jdt12.miniproject.controller.VehicleController;
import id.co.indivara.jdt12.miniproject.dto.request.AuthenticateRequest;
import id.co.indivara.jdt12.miniproject.dto.response.AuthenticationResponse;
import id.co.indivara.jdt12.miniproject.entity.Vehicle;
import id.co.indivara.jdt12.miniproject.service.VehicleService;
import id.co.indivara.jdt12.miniproject.utilize.mapper.MapperConvertion;
import id.co.indivara.jdt12.miniproject.utilize.response.ResponseMessage;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.Objects;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class VehicleControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private AuthenticationController authenticationController;
    private static ResponseEntity<AuthenticationResponse> response;
    @Autowired
    private VehicleController vehicleController;
    @Autowired
    private VehicleService vehicleService;

    @Before
    public void before() throws Exception {
        AuthenticateRequest authenticateRequest = new AuthenticateRequest(LoginData.adminUsername,LoginData.adminPassword);
        response = authenticationController.login(authenticateRequest);
        System.out.println(Objects.requireNonNull(response.getBody()).getAccessToken());
    }

    @Test
    public void getAllVehicleTest() throws Exception {
        List<Vehicle> vehiclesChecker = vehicleService.getAll();
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/vehicle")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization","Bearer "+ Objects.requireNonNull(response.getBody()).getAccessToken())
                )
                .andDo(result -> {
                    List<Vehicle> vehicles = MapperConvertion.getAllData(result.getResponse().getContentAsString(), Vehicle.class);
                    Assertions.assertNotNull(vehicles);
                    Assertions.assertEquals(vehiclesChecker.get(0).getName(), vehicles.get(0).getName());
                })
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNotEmpty());
    }

    @Test
    public void unGetAllVehicleTest() throws Exception {
        List<Vehicle> vehiclesChecker = vehicleService.getAll();
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/vehicle")
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isForbidden());
    }
    @Test
    public void getAllAvailableVehicleTest() throws Exception {
        List<Vehicle> vehiclesChecker = vehicleService.getAvailableVehicle(6,2,50000.0);
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/vehicle/available?seat=6&baggage=2&cost=50000")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization","Bearer "+ Objects.requireNonNull(response.getBody()).getAccessToken())
                )
                .andDo(result -> {
                    List<Vehicle> vehicles = MapperConvertion.getAllData(result.getResponse().getContentAsString(), Vehicle.class);
                    Assertions.assertNotNull(vehicles);
                    Assertions.assertEquals(vehiclesChecker.get(0).getName(), vehicles.get(0).getName());
                })
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(vehiclesChecker.get(0).getId()));

    }
    @Test
    public void getVehicleById() throws Exception {
        Vehicle vehicle = vehicleService.findById(1L);
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/vehicle/1")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization","Bearer "+ Objects.requireNonNull(response.getBody()).getAccessToken())
                )
                .andDo(result -> {
                    Vehicle vehicles = MapperConvertion.getData(result.getResponse().getContentAsString(), Vehicle.class);
                    Assertions.assertNotNull(vehicles);
                    Assertions.assertEquals(vehicle.getName(),vehicles.getName());
                })
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty());
    }

    @Test
    public void updateVehicle() throws Exception {
        Vehicle vehicle = vehicleService.findById(1L);
        mockMvc.perform(MockMvcRequestBuilders
                        .put("/api/vehicle/1")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertion.toJson(vehicle))
                        .header("Authorization","Bearer "+ Objects.requireNonNull(response.getBody()).getAccessToken())
                )
                .andDo(result -> {
                    Vehicle vehicles = MapperConvertion.getData(result.getResponse().getContentAsString(), Vehicle.class);
                    Assertions.assertNotNull(vehicles);
                    Assertions.assertEquals(vehicles.getName(),vehicle.getName());
                })
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty());
    }
    @Test
    public void unAuthorizeUpdateVehicle() throws Exception {
        Vehicle vehicle = vehicleService.findById(1L);
        mockMvc.perform(MockMvcRequestBuilders
                        .put("/api/vehicle/1")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertion.toJson(vehicle))
                )
                .andExpect(status().isForbidden());

    }
    @Test
    public void addAndDuplicateAndDeleteVehicle() throws Exception {
        Vehicle vehicle = Vehicle.builder()
                .name("Bugatti")
                .baggage(1)
                .level(1)
                .plate("B 9292 JT")
                .year(2020)
                .seat(2)
                .isAvailable(true)
                .rentCostPerHour(100000.0)
                .build();
        MvcResult vehicleResult =  mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/vehicle")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertion.toJson(vehicle))
                        .header("Authorization","Bearer "+ Objects.requireNonNull(response.getBody()).getAccessToken())
                )
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty()).andReturn();
                Vehicle vehicles = MapperConvertion.getData(vehicleResult.getResponse().getContentAsString(), Vehicle.class);
                Assertions.assertNotNull(vehicles);
                Assertions.assertEquals(vehicles.getName(),vehicle.getName());
                duplicateVehicle();
                deleteVehicle(vehicles.getId());
    }
    public void duplicateVehicle() throws Exception {
        Vehicle vehicle = Vehicle.builder()
                .name("Bugatti")
                .baggage(1)
                .level(1)
                .plate("B 9292 JT")
                .year(2020)
                .seat(2)
                .isAvailable(true)
                .rentCostPerHour(100000.0)
                .build();
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/vehicle")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertion.toJson(vehicle))
                        .header("Authorization","Bearer "+ Objects.requireNonNull(response.getBody()).getAccessToken())
                )
                .andExpect(status().isConflict())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value("CONFLICT"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.error").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.error").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.error").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.error[0]").value("Conflict"))

        ;
    }
    @Test
    public void unAuthorizeAddAndDeleteVehicle() throws Exception {
        Vehicle vehicle = Vehicle.builder()
                .name("lamborghini")
                .baggage(1)
                .level(1)
                .plate("B 1004 JT")
                .year(2020)
                .seat(2)
                .isAvailable(true)
                .rentCostPerHour(100000.0)
                .build();
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/vehicle")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertion.toJson(vehicle))
                )
                .andExpect(status().isForbidden());

    }
    public void deleteVehicle(Long id) throws Exception {

        mockMvc.perform(MockMvcRequestBuilders
                        .delete("/api/vehicle/"+id)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization","Bearer "+ Objects.requireNonNull(response.getBody()).getAccessToken())
                )
                .andDo(result -> {
                    ResponseMessage responseMessage = MapperConvertion.getData(result.getResponse().getContentAsString(), ResponseMessage.class);
                    Assertions.assertNotNull(responseMessage);
                    Assertions.assertEquals("Data Berhasil Dihapus",responseMessage.getMessage());
                })
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").isNotEmpty());
    }
}
