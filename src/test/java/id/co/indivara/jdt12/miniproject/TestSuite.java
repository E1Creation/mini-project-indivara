package id.co.indivara.jdt12.miniproject;

import com.googlecode.junittoolbox.SuiteClasses;
import com.googlecode.junittoolbox.WildcardPatternSuite;
import org.junit.runner.RunWith;

@RunWith(WildcardPatternSuite.class)
@SuiteClasses({
        "**/*Test.class"
})
public class TestSuite {
}
