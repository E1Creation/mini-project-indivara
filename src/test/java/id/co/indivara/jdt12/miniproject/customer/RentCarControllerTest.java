package id.co.indivara.jdt12.miniproject.customer;

import id.co.indivara.jdt12.miniproject.LoginData;
import id.co.indivara.jdt12.miniproject.controller.AuthenticationController;
import id.co.indivara.jdt12.miniproject.controller.RentCarController;
import id.co.indivara.jdt12.miniproject.dto.request.AuthenticateRequest;
import id.co.indivara.jdt12.miniproject.dto.request.RentCarRequest;
import id.co.indivara.jdt12.miniproject.dto.response.AuthenticationResponse;
import id.co.indivara.jdt12.miniproject.entity.RentCar;
import id.co.indivara.jdt12.miniproject.entity.Transaction;
import id.co.indivara.jdt12.miniproject.service.RentCarService;
import id.co.indivara.jdt12.miniproject.service.TransactionService;
import id.co.indivara.jdt12.miniproject.utilize.mapper.MapperConvertion;
import id.co.indivara.jdt12.miniproject.utilize.response.ResponseMessage;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.Objects;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class RentCarControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private AuthenticationController authenticationController;
    @Autowired
    private TransactionService transacionService;
    private static ResponseEntity<AuthenticationResponse> response;
    @Autowired
    private RentCarController rentcarController;
    @Autowired
    private RentCarService rentcarService;

    @Before
    public void before() throws Exception {
        AuthenticateRequest authenticateRequest = new AuthenticateRequest(LoginData.customerUsername,LoginData.customerPassword);
        response = authenticationController.login(authenticateRequest);
        System.out.println(Objects.requireNonNull(response.getBody()).getAccessToken());
    }
    @Test
    public void unAuthorizeGetAllRentcarTest() throws Exception {
        List<RentCar> rentCarList = rentcarService.getAll();
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/rentcar")
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isForbidden());
    }
    @Test
    public void unAuthorizeGetRentcarById() throws Exception {
        RentCar rentcar = rentcarService.findById(1L);
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/rentcar/1")
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isForbidden());
    }
    @Test
    public void unAuthorizeUpdateRentcar() throws Exception {
        RentCar rentcar = rentcarService.findById(1L);
        mockMvc.perform(MockMvcRequestBuilders
                        .put("/api/rentcar/1")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertion.toJson(rentcar))
                )
                .andExpect(status().isForbidden());

    }
    @Test
    public void unAuthorizeAddAndFinishingTransactionAndDeleteRentcar() throws Exception {
        RentCarRequest rentcar = RentCarRequest.builder()
                .customerId(7L)
                .vehicleId(1L)
                .build();
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/rentcar/driver")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertion.toJson(rentcar))
                )
                .andExpect(status().isForbidden());

    }

    @Test
    public void getAllRentcarTest() throws Exception {
        List<RentCar> rentCarList = rentcarService.getAll();
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/rentcar")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization","Bearer "+ Objects.requireNonNull(response.getBody()).getAccessToken())
                )
                .andExpect(status().isForbidden())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());

    }
    @Test
    public void getRentcarById() throws Exception {
        RentCar rentcar = rentcarService.findById(1L);
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/rentcar/1")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization","Bearer "+ Objects.requireNonNull(response.getBody()).getAccessToken())
                )
                .andDo(result -> {
                    RentCar rentcars = MapperConvertion.getData(result.getResponse().getContentAsString(), RentCar.class);
                    Assertions.assertNotNull(rentcars);
                    Assertions.assertEquals(rentcar.getId(),rentcars.getId());
                })
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty());
    }
    @Test
    public void updateRentcar() throws Exception {
        RentCar rentcar = rentcarService.findById(1L);
        mockMvc.perform(MockMvcRequestBuilders
                        .put("/api/rentcar/1")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertion.toJson(rentcar))
                        .header("Authorization","Bearer "+ Objects.requireNonNull(response.getBody()).getAccessToken())
                )
                .andExpect(status().isForbidden())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());
    }
    @Test
    public void addAndFinishingTransactionAndDeleteRentcar() throws Exception {
        RentCarRequest rentcar = RentCarRequest.builder()
                .customerId(7L)
                .vehicleId(1L)
                .build();
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/rentcar/driver")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertion.toJson(rentcar))
                        .header("Authorization","Bearer "+ Objects.requireNonNull(response.getBody()).getAccessToken())
                )
                .andDo(result -> {
                    RentCar rentcars = MapperConvertion.getData(result.getResponse().getContentAsString(), RentCar.class);
                    Assertions.assertNotNull(rentcars);
                    Assertions.assertEquals(rentcar.getVehicleId(),rentcars.getVehicle().getId());
                    System.out.println("rent car " +rentcars);
                    finishingTransaction(rentcars.getId());
                    deleteRentcar(rentcars.getId());
                })
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty());
    }
    public void finishingTransaction(Long id) throws Exception {
        Transaction transaction = transacionService.findById(id);
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/transaction/"+id+"/rent")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization","Bearer "+ Objects.requireNonNull(response.getBody()).getAccessToken())
                )
                .andDo(result -> {
                    Transaction transactions = MapperConvertion.getData(result.getResponse().getContentAsString(), Transaction.class);
                    Assertions.assertNotNull(transactions);
                    Assertions.assertEquals(transactions.getId(),transaction.getId());
                })
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty());
    }
    public void deleteRentcar(Long id) throws Exception {

        mockMvc.perform(MockMvcRequestBuilders
                        .delete("/api/rentcar/"+id)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization","Bearer "+ Objects.requireNonNull(response.getBody()).getAccessToken())
                )
                .andExpect(status().isForbidden())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());
    }
}
