package id.co.indivara.jdt12.miniproject.customer;

import id.co.indivara.jdt12.miniproject.LoginData;
import id.co.indivara.jdt12.miniproject.controller.AuthenticationController;
import id.co.indivara.jdt12.miniproject.controller.TransactionController;
import id.co.indivara.jdt12.miniproject.dto.request.AuthenticateRequest;
import id.co.indivara.jdt12.miniproject.dto.response.AuthenticationResponse;
import id.co.indivara.jdt12.miniproject.entity.Transaction;
import id.co.indivara.jdt12.miniproject.service.TransactionService;
import id.co.indivara.jdt12.miniproject.utilize.mapper.MapperConvertion;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.Objects;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class TransactionControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private AuthenticationController authenticationController;
    private static ResponseEntity<AuthenticationResponse> response;
    @Autowired
    private TransactionController transactionController;
    @Autowired
    private TransactionService transactionService;

    @Before
    public void before() throws Exception {
        AuthenticateRequest authenticateRequest = new AuthenticateRequest(LoginData.customerUsername,LoginData.customerPassword);
        response = authenticationController.login(authenticateRequest);
        System.out.println(Objects.requireNonNull(response.getBody()).getAccessToken());
    }
    @Test
    public void unAuthorizeGetAllTransactionTest() throws Exception {
        List<Transaction> transactionList = transactionService.getAll();
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/transaction")
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isForbidden());


    }
    @Test
    public void unAuthorizeGetTransactionById() throws Exception {
        Transaction transaction = transactionService.findById(1L);
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/transaction/1")
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isForbidden());

    }
    @Test
    public void getAllTransactionTest() throws Exception {
        List<Transaction> transactionList = transactionService.getAll();
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/transaction")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization","Bearer "+ Objects.requireNonNull(response.getBody()).getAccessToken())
                )
                .andExpect(status().isForbidden())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());

    }

    @Test
    public void getTransactionById() throws Exception {
        Transaction transaction = transactionService.findById(1L);
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/transaction/1")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization","Bearer "+ Objects.requireNonNull(response.getBody()).getAccessToken())
                )
                .andDo(result -> {
                    Transaction transactions = MapperConvertion.getData(result.getResponse().getContentAsString(), Transaction.class);
                    Assertions.assertNotNull(transactions);
                    Assertions.assertEquals(transaction.getId(),transactions.getId());
                })
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty());
    }


}
