package id.co.indivara.jdt12.miniproject.admin;

import com.fasterxml.jackson.core.JsonProcessingException;
import id.co.indivara.jdt12.miniproject.LoginData;
import id.co.indivara.jdt12.miniproject.controller.AuthenticationController;
import id.co.indivara.jdt12.miniproject.dto.request.AuthenticateRequest;
import id.co.indivara.jdt12.miniproject.dto.request.RegisterWithDriverRole;
import id.co.indivara.jdt12.miniproject.dto.request.RegisterWithRole;
import id.co.indivara.jdt12.miniproject.dto.response.AuthenticationResponse;
import id.co.indivara.jdt12.miniproject.entity.Gender;
import id.co.indivara.jdt12.miniproject.entity.User;
import id.co.indivara.jdt12.miniproject.service.AuthenticationService;
import id.co.indivara.jdt12.miniproject.service.JwtService;
import id.co.indivara.jdt12.miniproject.service.UserService;
import id.co.indivara.jdt12.miniproject.utilize.mapper.MapperConvertion;
import id.co.indivara.jdt12.miniproject.utilize.response.ResponseMessage;
import io.jsonwebtoken.Claims;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AuthenticationControllerTest {
    @Autowired
    MockMvc mockMvc;
    @Autowired
    private JwtService jwtService;
    @Autowired
    private UserService userService;
    @Autowired
    private  AuthenticationController authenticationController;
    @Autowired
    private AuthenticationService authenticationService;
    private String jwtToken;
    @Before
    public void setup(){
        String username = LoginData.adminUsername;
        String password = LoginData.adminPassword;
        AuthenticateRequest authenticateRequest = new AuthenticateRequest(username,password);
        ResponseEntity<AuthenticationResponse> response = authenticationController.login(authenticateRequest);
        setJwtToken(Objects.requireNonNull(response.getBody()).getAccessToken());
        assertNotNull(response);
        System.out.println("Menyiapkan setup login");
    }
    @Test
    public void loginAsAdminTest() throws Exception {
        AuthenticateRequest authenticateRequest = new AuthenticateRequest("admin","admin");
        mockMvc.perform(MockMvcRequestBuilders
                       .post("/api/auth/login")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(MapperConvertion.toJson(authenticateRequest))
        ).andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.access_token").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.access_token").exists());
    }
    @Test
    public void loginAsAdminButBadCredentialsTest() throws Exception {
        AuthenticateRequest authenticateRequest = new AuthenticateRequest("admins","admins");
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/auth/login")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertion.toJson(authenticateRequest))
                ).andExpect(status().isForbidden())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.access_token").doesNotExist())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value("FORBIDDEN"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Bad credentials"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.error").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.error").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.error").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.error[0]").value("Username atau password salah"));
    }
    @Test
    public void loginAsCustomerTest() throws Exception {
        AuthenticateRequest authenticateRequest = new AuthenticateRequest("customer","customer");
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/auth/login")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertion.toJson(authenticateRequest))
                ).andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.access_token").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.access_token").exists());
    }
    @Test
    public void loginAsCustomerButBadCredentialsTest() throws Exception {
        AuthenticateRequest authenticateRequest = new AuthenticateRequest("customers","customers");
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/auth/login")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertion.toJson(authenticateRequest))
                ).andExpect(status().isForbidden())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.access_token").doesNotExist())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value("FORBIDDEN"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Bad credentials"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.error").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.error").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.error").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.error[0]").value("Username atau password salah"));
    }
    @Test
    public void loginAsDriverTest() throws Exception {
        AuthenticateRequest authenticateRequest = new AuthenticateRequest("driver0","driver0");
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/auth/login")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertion.toJson(authenticateRequest))
                ).andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.access_token").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.access_token").exists());
    }
    @Test
    public void loginAsDriverButBadCredentialsTest() throws Exception {
        AuthenticateRequest authenticateRequest = new AuthenticateRequest("driver0001","driver0001");
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/auth/login")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertion.toJson(authenticateRequest))
                ).andExpect(status().isForbidden())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.access_token").doesNotExist())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value("FORBIDDEN"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Bad credentials"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.error").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.error").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.error").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.error[0]").value("Username atau password salah"));
    }
    @Test
    public void registrasiAndErrorTestAndDeleteCustomer() throws Exception {
        RegisterWithRole registerWithRole = RegisterWithRole.builder()
                .username("betatester")
                .password("betatester")
                .email("betatester@gmail.com")
                .fullName("betatester")
                .gender(Gender.Male)
                .phoneNumber("081273828291")
                .build();
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/auth/register")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertion.toJson(registerWithRole))
                )
                .andDo(result -> {
                    AuthenticationResponse users = MapperConvertion.getData(result.getResponse().getContentAsString(), AuthenticationResponse.class);
                    Assertions.assertNotNull(users);
                    Assertions.assertNotNull(users.getAccessToken());
                    duplicateKeyCustomerExceptionTest();
                    User user = authenticationService.findByUsername(jwtService.extractClaim(users.getAccessToken(),Claims::getSubject));
                    deleteUser(user.getId());
                })
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.access_token").isNotEmpty());
    }
    @Test
    public void registrasiAndErrorTestAndDeleteDriver() throws Exception {
        RegisterWithDriverRole registerWithRole = new RegisterWithDriverRole("betadriver","betadriver","beta driver","betadriver@gmail.com","081285012939",Gender.Female,"291938123912");
        MvcResult register = mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/auth/register/driver")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertion.toJson(registerWithRole))
                )

                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.access_token").isNotEmpty())
                .andReturn();
                AuthenticationResponse users = MapperConvertion.getData(register.getResponse().getContentAsString(), AuthenticationResponse.class);
                Assertions.assertNotNull(users);
                Assertions.assertNotNull(users.getAccessToken());
                User user = authenticationService.findByUsername(jwtService.extractClaim(users.getAccessToken(),Claims::getSubject));
                approvalDriver(user.getId());
                duplicateKeyDriverExceptionTest();
                deleteUser(user.getId());
    }


    public void duplicateKeyDriverExceptionTest() throws Exception {
        RegisterWithDriverRole registerWithRole = new RegisterWithDriverRole("betadriver","betadriver","beta driver","betadriver@gmail.com","081285012939",Gender.Female,"291938123912");
        mockMvc.perform(MockMvcRequestBuilders
                       .post("/api/auth/register/driver")
                       .accept(MediaType.APPLICATION_JSON)
                       .contentType(MediaType.APPLICATION_JSON)
                       .content(MapperConvertion.toJson(registerWithRole))
                ).andExpect(status().isConflict())
//                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("ERROR: duplicate key value violates unique constraint \"uk_r43af9ap4edm43mmtq01oddj6\"\\n  Detail: Key (username)=(betadriver) already exists."))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value("CONFLICT"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.error[0]").value("Conflict"));
    }

    public void duplicateKeyCustomerExceptionTest() throws Exception {
        RegisterWithRole registerWithRole = RegisterWithRole.builder()
                .username("betatester")
                .password("betatester")
                .email("betatester@gmail.com")
                .fullName("betatester")
                .gender(Gender.Male)
                .phoneNumber("081273828291")
                .build();
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/auth/register")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertion.toJson(registerWithRole))
                ).andExpect(status().isConflict())
//                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("ERROR: duplicate key value violates unique constraint \"uk_r43af9ap4edm43mmtq01oddj6\"\\n  Detail: Key (username)=(betatester) already exists."))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value("CONFLICT"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.error[0]").value("Conflict"));
    }
    public void approvalDriver(Long id) throws Exception {
        User userChecker = userService.findById(id);
        mockMvc.perform(MockMvcRequestBuilders
                       .get("/api/auth/approval/"+id)
                       .accept(MediaType.APPLICATION_JSON)
                       .contentType(MediaType.APPLICATION_JSON)
                       .header("Authorization","Bearer "+ Objects.requireNonNull(getJwtToken())
                ))
               .andDo(result -> {
                    User user = MapperConvertion.getData(result.getResponse().getContentAsString(), User.class);
                    Assertions.assertNotNull(user);
                    Assertions.assertEquals(userChecker.getId(),user.getId());
                })
               .andExpect(status().isAccepted())
               .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
               .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
               .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(userChecker.getId()))
        ;
    }
    public void deleteUser(Long id) throws Exception {

        mockMvc.perform(MockMvcRequestBuilders
                        .delete("/api/user/"+id)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization","Bearer "+ Objects.requireNonNull(getJwtToken())
                ))
                .andDo(result -> {
                    ResponseMessage responseMessage = MapperConvertion.getData(result.getResponse().getContentAsString(), ResponseMessage.class);
                    Assertions.assertNotNull(responseMessage);
                    Assertions.assertEquals("Data Berhasil Dihapus",responseMessage.getMessage());
                })
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").isNotEmpty());
    }
    public String getJwtToken() {
        return jwtToken;
    }

    public void setJwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
    }
}
