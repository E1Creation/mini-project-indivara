package id.co.indivara.jdt12.miniproject.driver;


import id.co.indivara.jdt12.miniproject.LoginData;
import id.co.indivara.jdt12.miniproject.controller.AuthenticationController;
import id.co.indivara.jdt12.miniproject.controller.CustomerController;
import id.co.indivara.jdt12.miniproject.dto.request.AuthenticateRequest;
import id.co.indivara.jdt12.miniproject.dto.response.AuthenticationResponse;
import id.co.indivara.jdt12.miniproject.entity.Customer;
import id.co.indivara.jdt12.miniproject.service.CustomerService;
import id.co.indivara.jdt12.miniproject.utilize.mapper.MapperConvertion;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.Objects;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CustomerControllerTest  {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private AuthenticationController authenticationController;
    private static ResponseEntity<AuthenticationResponse> response;
    @Autowired
    private  CustomerController customerController;
    @Autowired
    private CustomerService customerService;

    @Before
    public void before() throws Exception {
        AuthenticateRequest authenticateRequest = new AuthenticateRequest(LoginData.driverUsername,LoginData.driverPassword);
        response = authenticationController.login(authenticateRequest);
        System.out.println(Objects.requireNonNull(response.getBody()).getAccessToken());
    }
    @Test
    public void unAuthorizeGetAllCustomer() throws Exception {
        List<Customer> customerList = customerService.getAll();
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/customer")
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isForbidden());
    }
    @Test
    public void unAuthorizeGetCustomerById() throws Exception {
        Customer customer = customerService.findById(7L);
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/customer/7")
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isForbidden());
    }
    @Test
    public void unAuthorizeUpdateCustomer() throws Exception {
        Customer customer = customerService.findById(7L);
        mockMvc.perform(MockMvcRequestBuilders
                        .put("/api/customer/7")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertion.toJson(customer))
                )
                .andExpect(status().isForbidden());
    }
    @Test
    public void getAllCustomerTest() throws Exception {
        List<Customer> customerList = customerService.getAll();
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/customer")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization","Bearer "+ Objects.requireNonNull(response.getBody()).getAccessToken())
                )
                .andExpect(status().isForbidden())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());

    }
    @Test
    public void getCustomerById() throws Exception {
        Customer customer = customerService.findById(7L);
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/customer/7")
                        .accept(MediaType.APPLICATION_JSON)
                        .header("Authorization","Bearer "+ Objects.requireNonNull(response.getBody()).getAccessToken())
                )
                .andExpect(status().isForbidden())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());
    }
    @Test
    public void updateCustomer() throws Exception {
        Customer customer = customerService.findById(7L);
        mockMvc.perform(MockMvcRequestBuilders
                        .put("/api/customer/7")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(MapperConvertion.toJson(customer))
                        .header("Authorization","Bearer "+ Objects.requireNonNull(response.getBody()).getAccessToken())
                )
                .andExpect(status().isForbidden())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());
    }

}
