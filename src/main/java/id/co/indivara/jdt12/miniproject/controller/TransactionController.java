package id.co.indivara.jdt12.miniproject.controller;

import id.co.indivara.jdt12.miniproject.entity.Transaction;
import id.co.indivara.jdt12.miniproject.repo.TransactionRepository;
import id.co.indivara.jdt12.miniproject.service.TransactionService;
import id.co.indivara.jdt12.miniproject.utilize.response.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/api/transaction")
public class TransactionController extends GenericController<Transaction>{
    @Autowired
    private TransactionService transactionService;

    @GetMapping("/{id}/rent")
    public ResponseEntity<Transaction> finishTheRent(@PathVariable Long id){
        return new ResponseEntity<>(transactionService.finish(id), HttpStatus.OK);
    }

    @GetMapping("/customer/{id}")
    public ResponseEntity<List<Transaction>> findAllTransactionByCustomerId(@PathVariable Long id){
            return new ResponseEntity<>(transactionService.findAllTransactionByCustomerId(id), HttpStatus.OK);
    }
    @GetMapping("/driver/{id}")
    public ResponseEntity<List<Transaction>> findAllTransactionByDriverId(@PathVariable Long id){
        return new ResponseEntity<>(transactionService.findAllTransactionByDriverId(id), HttpStatus.OK);
    }

    @GetMapping("/driver/{id}/notification")
    public ResponseEntity<Transaction> findNotificationByDriverId(@PathVariable Long id){
        return new ResponseEntity<>(transactionService.notificationWork(id), HttpStatus.OK);
    }

    @GetMapping("/payment/{id}")
    public ResponseEntity<ResponseMessage> completePayment(@PathVariable Long id){
        return new ResponseEntity<>(transactionService.completePayment(id), transactionService.completePayment(id).getStatus());
    }
//    @GetMapping("/{transactionId}/customer/{id}")
//    public ResponseEntity<List<Transaction>> findAllTransactionByCustomerIdAndTransactionId(@PathVariable Long id, @PathVariable Long transactionId){
//        return new ResponseEntity<>(transactionService.findAllTransactionByCustomerIdAndTransactionId(id, transactionId), HttpStatus.OK);
//    }
//    @GetMapping("/{transactionId}/driver/{id}")
//    public ResponseEntity<List<Transaction>> findAllTransactionByDriverIdAndTransactionId(@PathVariable Long id, @PathVariable Long transactionId){
//        return new ResponseEntity<>(transactionService.findAllTransactionByDriverIdAndTransactionId(id, transactionId), HttpStatus.OK);
//    }
}
