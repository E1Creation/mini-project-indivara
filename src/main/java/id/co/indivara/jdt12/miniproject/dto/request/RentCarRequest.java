package id.co.indivara.jdt12.miniproject.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RentCarRequest {
    @JsonProperty("vehicle_id")
    private Long vehicleId;
    @JsonProperty("customer_id")
    private Long customerId;
    @JsonProperty("driver_id")
    private Long driverId;
}
