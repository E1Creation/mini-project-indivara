package id.co.indivara.jdt12.miniproject.dto.request;

import id.co.indivara.jdt12.miniproject.entity.Gender;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class RegisterWithDriverRole extends RegisterWithRole{

    private String licenceDriver;

    public RegisterWithDriverRole(String username, String password, String fullName, String email, String phoneNumber, Gender gender, String licenceDriver) {
        super(username, password, fullName, email, phoneNumber, gender);
        this.licenceDriver = licenceDriver;
    }
}
