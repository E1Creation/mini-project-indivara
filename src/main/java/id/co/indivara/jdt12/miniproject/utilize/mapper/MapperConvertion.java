package id.co.indivara.jdt12.miniproject.utilize.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import id.co.indivara.jdt12.miniproject.dto.request.RegisterWithDriverRole;
import id.co.indivara.jdt12.miniproject.dto.request.RegisterWithRole;
import id.co.indivara.jdt12.miniproject.entity.*;

import java.util.ArrayList;
import java.util.List;


public class MapperConvertion {
    public static User registerToUser(RegisterWithRole registerWithRole){
        return User.builder()
                .username(registerWithRole.getUsername())
                .password(registerWithRole.getPassword())
                .isEnabled(true)
                .isAccountLocked(false)
                .build();
    }
    public static Customer registertoCustomer(RegisterWithRole registerWithRole){
        Customer customer = new Customer();
        customer.setEmail(registerWithRole.getEmail());
        customer.setPhoneNumber(registerWithRole.getPhoneNumber());
        customer.setGender(registerWithRole.getGender());
        customer.setName(registerWithRole.getFullName());
        return customer;
    }
    public static Driver registertoDriver(RegisterWithDriverRole registerWithDriverRole){
        Driver driver = new Driver();
        driver.setEmail(registerWithDriverRole.getEmail());
        driver.setPhoneNumber(registerWithDriverRole.getPhoneNumber());
        driver.setGender(registerWithDriverRole.getGender());
        driver.setName(registerWithDriverRole.getFullName());
        driver.setLicenceDriver(registerWithDriverRole.getLicenceDriver());
        return driver;
    }

    public static  List<Customer> getAllDataCustomer(String json) throws JsonProcessingException {
        return new ObjectMapper().readValue(json, new TypeReference<List<Customer>>() {
        });
    }
    public static <T> List<T> getAllData(String json, Class<T> elementClass)throws JsonProcessingException{
        ObjectMapper objectMapper = new ObjectMapper();
        CollectionType listType =
                objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, elementClass);
        return objectMapper.readValue(json, listType);
    }
    public static <T> T getData(String json, Class<T> tclass) throws  JsonProcessingException{
        ObjectMapper ob = new ObjectMapper();

        return ob.readValue(json, tclass);
    }
    public static <T> String toJson(T object) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(object);
    }

 }
