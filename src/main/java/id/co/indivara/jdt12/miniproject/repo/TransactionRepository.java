package id.co.indivara.jdt12.miniproject.repo;


import id.co.indivara.jdt12.miniproject.entity.Transaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface TransactionRepository extends GenericRepository<Transaction> {

    @Query(value = "SELECT t.id, t.duration, t.end_rent, t.is_already_paid, t.start_rent, t.total FROM transaction t, rent_car r WHERE t.id=r.id and r.customer=?1",nativeQuery = true)
    List<Transaction> findAllTransactionByCustomerId(Long id);
    @Query(value = "SELECT t.id, t.duration, t.end_rent, t.is_already_paid, t.start_rent, t.total FROM transaction t, rent_car r WHERE t.id=r.id and r.driver=?1",nativeQuery = true)
    List<Transaction> findAllTransactionByDriverId(Long id);
    @Query(value = "SELECT t.id, t.duration, t.end_rent, t.is_already_paid, t.start_rent, t.total FROM transaction t, rent_car r WHERE t.id=r.id and r.driver=?1 and t.id=?2",nativeQuery = true)
    List<Transaction> findAllTransactionByDriverIdAndTransaksionId(Long id, Long transactionId);
    @Query(value = "SELECT t.id, t.duration, t.end_rent, t.is_already_paid, t.start_rent, t.total FROM transaction t, rent_car r WHERE t.id=r.id and r.customer=?1 and t.id=?2",nativeQuery = true)
    List<Transaction> findAllTransactionByCustomerIdAndTransactionId(Long id, Long transactionId);
}
