package id.co.indivara.jdt12.miniproject.service;

import id.co.indivara.jdt12.miniproject.entity.Vehicle;
import id.co.indivara.jdt12.miniproject.repo.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class VehicleService extends GenericService<Vehicle> {

    @Autowired
    private VehicleRepository vehicleRepository;

    public List<Vehicle> getAvailableVehicle(Integer seat, Integer baggage, Double cost){
        if(seat == null && baggage == null && cost == null) return vehicleRepository.getAvailableVehicle();
        return vehicleRepository.getAvailableVehicle().stream()
                .filter(vehicle -> (seat == null || Objects.equals(vehicle.getSeat(), seat)))
                .filter(vehicle -> baggage == null || Objects.equals(vehicle.getBaggage(), baggage))
                .filter(vehicle -> vehicle.getRentCostPerHour() >= (cost == null ? 0 : cost))
                .collect(Collectors.toList());
    }
}
