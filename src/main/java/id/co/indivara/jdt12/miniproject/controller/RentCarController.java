package id.co.indivara.jdt12.miniproject.controller;


import id.co.indivara.jdt12.miniproject.dto.request.RentCarRequest;
import id.co.indivara.jdt12.miniproject.entity.RentCar;
import id.co.indivara.jdt12.miniproject.service.RentCarService;
import id.co.indivara.jdt12.miniproject.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@RestController
@RequestMapping("/api/rentcar")
public class RentCarController extends GenericController<RentCar>{
    @Autowired
    private RentCarService rentCarService;

    @PostMapping("/nodriver")
    public ResponseEntity<RentCar> insert(@RequestBody RentCarRequest entity) {
        return new ResponseEntity<>(rentCarService.saveWithoutDriver(entity), HttpStatus.CREATED);
    }

    @PostMapping("/driver")
    public ResponseEntity<RentCar> saveWithDriver(@RequestBody RentCarRequest rentCar){
        return new ResponseEntity<>(rentCarService.saveWithDriver(rentCar), HttpStatus.CREATED);
    }

    @GetMapping("/driver/{id}")
    public ResponseEntity<List<RentCar>> getAllRentCarDriverById(@PathVariable Long id){
        return new ResponseEntity<>(rentCarService.getAllDataDriverById(id), HttpStatus.OK);
    }
    @GetMapping("/customer/{id}")
    public ResponseEntity<List<RentCar>> getAllRentCarCustomerrById(@PathVariable Long id){
        return new ResponseEntity<>(rentCarService.getAllDataCustomerById(id), HttpStatus.OK);
    }
}
