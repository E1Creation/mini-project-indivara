package id.co.indivara.jdt12.miniproject.config;

import id.co.indivara.jdt12.miniproject.dto.request.RegisterWithDriverRole;
import id.co.indivara.jdt12.miniproject.dto.request.RegisterWithRole;
import id.co.indivara.jdt12.miniproject.dto.request.RentCarRequest;
import id.co.indivara.jdt12.miniproject.entity.*;
import id.co.indivara.jdt12.miniproject.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class Data {

    @Autowired
    private AuthenticationService autuhenticationService;

    @Autowired
    private VehicleService vehicleService;

    @Autowired
    private RentCarService rentCarService;

    @Autowired
    private UserService userService;
    @Autowired
    private DriverService driverService;
    @Autowired
    private TransactionService transactionService;
    @Bean
    public void initializeData(){
        if(userService.getAll().isEmpty()){
            RegisterWithRole admin = RegisterWithRole
                    .builder()
                    .username("admin")
                    .password("admin")
                    .phoneNumber("0812345678910")
                    .gender(Gender.Male)
                    .email("admin@gmail.com")
                    .fullName("administrator")
                    .build();

            RegisterWithRole customer =RegisterWithRole
                    .builder()
                    .username("customer")
                    .password("customer")
                    .phoneNumber("0812345678910")
                    .gender(Gender.Male)
                    .email("customer@gmail.com")
                    .fullName("customer")
                    .build();
            for(int i = 0; i < 5; i++){
                RegisterWithDriverRole driver = new RegisterWithDriverRole(
                        "driver"+i,"driver"+i,
                        "driver"+i,"driver"+i+"@gmail.com",
                        "08127318123"+i,Gender.Male,
                        "12381239129"+i
                );
                autuhenticationService.registerWithDriverRole(driver);
            }
            autuhenticationService.registerWithAdminRole(admin);
            autuhenticationService.registerWithCustomerRole(customer);
        }
        if(!driverService.getAll().get(0).getIsAvailable()){
            for(Driver driver: driverService.getAll()) autuhenticationService.approvalDriver(driver.getId());
        }
        List<Map<String, Object>> mobilList = new ArrayList<Map<String, Object>>() {{
            add(new HashMap<String, Object>() {{
                put("nama", "Avanza");
                put("sewaPerJam", 15000.0);
                put("tempatDuduk", 6);
                put("level",3);
                put("bagasi",1);
            }});
            add(new HashMap<String,Object>() {{
                put("nama", "Innova");
                put("sewaPerJam", 30000.0);
                put("tempatDuduk", 6);
                put("level",3);
                put("bagasi",2);

            }});
            add(new HashMap<String,Object>() {{
                put("nama", "Ertiga");
                put("sewaPerJam", 20000.0);
                put("tempatDuduk", 6);
                put("level",2);
                put("bagasi",1);

            }});
            add(new HashMap<String, Object>() {{
                put("nama", "mercedez");
                put("sewaPerJam", 50000.0);
                put("tempatDuduk", 4);
                put("level",1);
                put("bagasi",1);

            }});
            add(new HashMap<String,Object>() {{
                put("nama", "agya");
                put("sewaPerJam", 12000.0);
                put("tempatDuduk", 4);
                put("level",3);
                put("bagasi",1);
            }});
            add(new HashMap<String,Object>() {{
                put("nama", "xenia");
                put("sewaPerJam", 15000.0);
                put("tempatDuduk", 6);
                put("level",3);
                put("bagasi",1);
            }});
            add(new HashMap<String, Object>() {{
                put("nama", "yaris");
                put("sewaPerJam", 10000.0);
                put("tempatDuduk", 4);
                put("level",3);
                put("bagasi",1);
            }});
            add(new HashMap<String,Object>() {{
                put("nama", "bmw");
                put("sewaPerJam", 45000.0);
                put("tempatDuduk", 4);
                put("level",1);
                put("bagasi",1);
            }});
            add(new HashMap<String,Object>() {{
                put("nama", "alphard");
                put("sewaPerJam", 55000.0);
                put("tempatDuduk", 6);
                put("level",1);
                put("bagasi",2);
            }});
        }};

        if(vehicleService.getAll().isEmpty()){
            int i = 0;
            for(Map<String,Object> mobil: mobilList){
                Vehicle vehicle = Vehicle.builder()
                        .name(mobil.get("nama").toString())
                        .plate("B 123"+i+" A")
                        .year(2019)
                        .seat((Integer) mobil.get("tempatDuduk"))
                        .baggage((Integer) mobil.get("bagasi"))
                        .level((Integer) mobil.get("level"))
                        .isAvailable(true)
                        .rentCostPerHour((Double) mobil.get("sewaPerJam"))
                        .build();
                vehicleService.save(vehicle);
                i++;
            }
        }
        if(rentCarService.getAll().isEmpty()){
            RentCarRequest rentCar = RentCarRequest.builder()
                    .vehicleId(1L)
                    .customerId(7L)
                    .build();
            rentCarService.saveWithDriver(rentCar);
        }


    }
}
