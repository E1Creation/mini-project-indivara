package id.co.indivara.jdt12.miniproject.service;

import id.co.indivara.jdt12.miniproject.dto.request.RegisterWithDriverRole;
import id.co.indivara.jdt12.miniproject.dto.request.RegisterWithRole;
import id.co.indivara.jdt12.miniproject.entity.Customer;
import id.co.indivara.jdt12.miniproject.entity.Driver;
import id.co.indivara.jdt12.miniproject.entity.Role;
import id.co.indivara.jdt12.miniproject.entity.User;
import id.co.indivara.jdt12.miniproject.repo.UserRepository;
import id.co.indivara.jdt12.miniproject.utilize.mapper.MapperConvertion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;


@Service
public class UserService extends GenericService<User> {

    @Autowired
    private UserRepository userRepository;

    public User saveWithCustomerRole(RegisterWithRole registerWithRole){
        User user = MapperConvertion.registerToUser(registerWithRole);
        Customer customer = MapperConvertion.registertoCustomer(registerWithRole);
        user.setRole(Role.CUSTOMER);
        customer.setUser(user);
        user.setCustomer(customer);
        return userRepository.save(user);
    }

    public User saveWithDriverRole(RegisterWithDriverRole registerWithDriverRole){
        User user = MapperConvertion.registerToUser(registerWithDriverRole);
        Driver driver = MapperConvertion.registertoDriver(registerWithDriverRole);
        driver.setIsAvailable(false);
        driver.setLevel(3);
        driver.setUser(user);
        user.setIsEnabled(false);
        user.setDriver(driver);
        return userRepository.save(user);
    }
    public User adminApproval(Long id){
        User user = this.findById(id);
        Driver driver = user.getDriver();
        if(user.getCustomer() == null && user.getDriver() == null) throw new NullPointerException("Maaf, id yang anda masukkan salah atau id tersebut role selain driver");
        user.setIsEnabled(true);
        driver.setIsAvailable(true);
        driver.setCostPerHour(10000.0);
        driver.setCountOfRented(0);
        driver.setUser(user);
        driver.setStartWork(new Date());
        user.setRole(Role.DRIVER);
        user.setDriver(driver);
        return userRepository.save(user);
    }
}
