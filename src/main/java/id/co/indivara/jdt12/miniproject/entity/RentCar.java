package id.co.indivara.jdt12.miniproject.entity;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RentCar {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ToString.Exclude
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @OneToOne(mappedBy = "rentCar",cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private Transaction transaction;
    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "vehicle")
    private Vehicle vehicle;
    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "customer")
    private Customer customer;
    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "driver")
    private Driver driver;
    private Boolean isCurrentlyRented;

}
