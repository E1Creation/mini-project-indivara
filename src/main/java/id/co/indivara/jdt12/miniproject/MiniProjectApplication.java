package id.co.indivara.jdt12.miniproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
public class MiniProjectApplication {
	public static void main(String[] args) {
		SpringApplication.run(MiniProjectApplication.class, args);
	}
}
