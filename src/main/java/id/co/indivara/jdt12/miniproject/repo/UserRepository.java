package id.co.indivara.jdt12.miniproject.repo;

import id.co.indivara.jdt12.miniproject.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends GenericRepository<User> {
    Optional<User> findByUsername(String username);
    Optional<User> findByToken(String token);
}
