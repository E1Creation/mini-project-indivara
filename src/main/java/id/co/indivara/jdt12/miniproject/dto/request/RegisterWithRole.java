package id.co.indivara.jdt12.miniproject.dto.request;

import id.co.indivara.jdt12.miniproject.entity.Gender;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RegisterWithRole {
    private String username;
    @NotBlank(message = "password tidak boleh kosong")
    @Size(min = 3,max = 50, message = "password minimal 3 huruf dan maks 50 huruf")
    private String password;
    private String fullName;
    private String email;
    private String phoneNumber;
    private Gender gender;
}
