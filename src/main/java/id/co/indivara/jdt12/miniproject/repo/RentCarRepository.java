package id.co.indivara.jdt12.miniproject.repo;

import id.co.indivara.jdt12.miniproject.entity.RentCar;
import id.co.indivara.jdt12.miniproject.entity.Vehicle;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface RentCarRepository extends GenericRepository<RentCar>{


}
