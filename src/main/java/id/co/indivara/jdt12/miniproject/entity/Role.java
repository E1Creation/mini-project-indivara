package id.co.indivara.jdt12.miniproject.entity;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.var;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.security.Permission;
import java.util.*;
import java.util.stream.Collectors;

public enum Role {

    CUSTOMER, DRIVER,ADMIN;

}
