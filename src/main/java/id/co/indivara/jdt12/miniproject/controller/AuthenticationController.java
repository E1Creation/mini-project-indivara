package id.co.indivara.jdt12.miniproject.controller;

import id.co.indivara.jdt12.miniproject.dto.request.AuthenticateRequest;
import id.co.indivara.jdt12.miniproject.dto.request.RegisterWithDriverRole;
import id.co.indivara.jdt12.miniproject.dto.request.RegisterWithRole;
import id.co.indivara.jdt12.miniproject.dto.response.AuthenticationResponse;
import id.co.indivara.jdt12.miniproject.entity.User;
import id.co.indivara.jdt12.miniproject.service.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthenticationController {
    private final AuthenticationService service;

    @PostMapping("/register")
    public ResponseEntity<AuthenticationResponse> registerWithCustomerRole(@RequestBody RegisterWithRole request){
        return new ResponseEntity<>(service.registerWithCustomerRole(request), HttpStatus.CREATED);
    }
    @PostMapping("/register/driver")
    public ResponseEntity<AuthenticationResponse> registerWithDriverRole(@RequestBody  RegisterWithDriverRole request){
        return new ResponseEntity<>(service.registerWithDriverRole(request), HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<AuthenticationResponse> login(@RequestBody AuthenticateRequest request){
        return  new ResponseEntity<>(service.authenticate(request), HttpStatus.OK);
    }

    @GetMapping("/approval/{id}")
    public ResponseEntity<User> approvalDriver(@PathVariable Long id){
        return new ResponseEntity<>(service.approvalDriver(id), HttpStatus.ACCEPTED);
    }

}
