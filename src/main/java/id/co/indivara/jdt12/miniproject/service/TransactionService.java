package id.co.indivara.jdt12.miniproject.service;

import id.co.indivara.jdt12.miniproject.entity.Driver;
import id.co.indivara.jdt12.miniproject.entity.RentCar;
import id.co.indivara.jdt12.miniproject.entity.Transaction;
import id.co.indivara.jdt12.miniproject.entity.Vehicle;
import id.co.indivara.jdt12.miniproject.error.FinishingTheRentException;
import id.co.indivara.jdt12.miniproject.repo.RentCarRepository;
import id.co.indivara.jdt12.miniproject.repo.TransactionRepository;
import id.co.indivara.jdt12.miniproject.utilize.response.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;


@Service
public class TransactionService extends GenericService<Transaction> {

    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private RentCarRepository rentCarRepository;

    @Override
    public Transaction update(Long id, Transaction entity) {

        return super.update(id, entity);
    }

    public Transaction finish(Long id){
        Transaction transaction = this.findById(id);
        if(!transaction.getRentCar().getIsCurrentlyRented()) throw new FinishingTheRentException("Maaf, Anda telah mengakhiri penyewaan mobil");
        RentCar rentCar = transaction.getRentCar();
        Driver driver = rentCar.getDriver();
        Vehicle vehicle = rentCar.getVehicle();
        Date startRent = transaction.getStartRent();
        Date endRent = new Date();
        transaction.setEndRent(endRent);
        Duration duration = Duration.between(startRent.toInstant(),endRent.toInstant());
//        int initialHours = endRent.getHour() >= startRent.getHour() ? ((endRent.getHour() - startRent.getHour()) == 0 ? 1 : (endRent.getHour() - startRent.getHour())) : (endRent.getHour() + 24) - startRent.getHour();
//        Integer totalHours = (24* totalDays) + initialHours;
//        Double totalPrice = rentCar.getDriver() == null ? totalHours * rentCar.getVehicle().getRentCostPerHour() : (totalHours * rentCar.getVehicle().getRentCostPerHour()) + (totalHours * rentCar.getDriver().getCostPerHour());
        Long totalHours = duration.toHours() == 0 ? 1 : duration.toHours();
        Double totalPrice = rentCar.getDriver() == null ? totalHours * rentCar.getVehicle().getRentCostPerHour() : (totalHours * rentCar.getVehicle().getRentCostPerHour()) + (totalHours * rentCar.getDriver().getCostPerHour());
        if(driver != null){
            driver.setIsAvailable(true);
            driver.setCountOfRented(driver.getCountOfRented()+1);
            rentCar.setDriver(driver);
        }

        vehicle.setIsAvailable(true);
        rentCar.setIsCurrentlyRented(false);
        rentCar.setVehicle(vehicle);
        transaction.setDuration(totalHours);
        transaction.setTotal(totalPrice);
        transaction.setRentCar(rentCar);
        return transactionRepository.save(transaction);
    }
    public List<Transaction> findAllTransactionByCustomerId(Long id){
        return transactionRepository.findAllTransactionByCustomerId(id);
    }
    public List<Transaction> findAllTransactionByDriverId(Long id){
        return transactionRepository.findAllTransactionByDriverId(id);
    }
    public Transaction notificationWork(Long id){
        return findAllTransactionByDriverId(id).stream().filter(transaction -> transaction.getRentCar().getIsCurrentlyRented()).findFirst().orElseThrow(() -> new NoSuchElementException("Belum ada Notifikasi Kerjaan"));
    }
    public ResponseMessage completePayment(Long id){
        Transaction transaction = this.findById(id);
        if(!transaction.getIsAlreadyPaid() && !transaction.getRentCar().getIsCurrentlyRented()){
            transaction.setIsAlreadyPaid(true);
            transactionRepository.save(transaction);
            return new ResponseMessage(HttpStatus.CREATED, "Tagihan telah dibayarkan");
        }
        if(transaction.getRentCar().getIsCurrentlyRented()) return new ResponseMessage(HttpStatus.CONFLICT, "Penyewaan mobil masih berjalan");
        return new ResponseMessage(HttpStatus.CONFLICT, "Tagihan telah dibayarkan");

    }

    public List<Transaction> findAllTransactionByCustomerIdAndTransactionId(Long id, Long transactionId){
        return transactionRepository.findAllTransactionByCustomerIdAndTransactionId(id, transactionId);
    }
    public List<Transaction> findAllTransactionByDriverIdAndTransactionId(Long id, Long transactionId){
        return transactionRepository.findAllTransactionByDriverIdAndTransaksionId(id, transactionId);
    }
}
