package id.co.indivara.jdt12.miniproject.controller;

import id.co.indivara.jdt12.miniproject.dto.request.RegisterWithDriverRole;
import id.co.indivara.jdt12.miniproject.dto.request.RegisterWithRole;
import id.co.indivara.jdt12.miniproject.entity.User;
import id.co.indivara.jdt12.miniproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/user")
public class UserController extends GenericController<User> {

    @Autowired
    private UserService userService;

    @PostMapping("/customer")
    public ResponseEntity<User> registerWithCustomerRole(@RequestBody @Valid RegisterWithRole registerWithRole){
        return new ResponseEntity<>(userService.saveWithCustomerRole(registerWithRole), HttpStatus.CREATED);
    }
    @PostMapping("/driver")
    public ResponseEntity<User> registerWithDriverRole(@RequestBody RegisterWithDriverRole registerWithDriverRole){
        return new ResponseEntity<>(userService.saveWithDriverRole(registerWithDriverRole), HttpStatus.CREATED);
    }

    @GetMapping("/approval/{id}")
    public ResponseEntity<User> approvalDriver(@PathVariable Long id){
        return new ResponseEntity<>(userService.adminApproval(id), HttpStatus.ACCEPTED);
    }
}
