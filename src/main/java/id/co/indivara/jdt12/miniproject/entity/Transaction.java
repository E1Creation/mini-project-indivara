package id.co.indivara.jdt12.miniproject.entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Transaction {

    @Id
    private Long id;
    @OneToOne
    @MapsId
    @JoinColumn(name = "id")
    private RentCar rentCar;
    @JsonFormat(pattern = "dd-mm-yyyy HH:mm:ss", timezone = "Asia/Jakarta")
    private Date startRent;
    @JsonFormat(pattern = "dd-mm-yyyy HH:mm:ss", timezone = "Asia/Jakarta")
    private Date endRent;
    private Long duration;
    private Double total;
    private Boolean isAlreadyPaid;


}
