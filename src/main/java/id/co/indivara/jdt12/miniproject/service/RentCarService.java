package id.co.indivara.jdt12.miniproject.service;

import id.co.indivara.jdt12.miniproject.dto.request.RentCarRequest;
import id.co.indivara.jdt12.miniproject.entity.*;
import id.co.indivara.jdt12.miniproject.error.CheckAvailableVehicleAndDriverException;
import id.co.indivara.jdt12.miniproject.repo.CustomerRepository;
import id.co.indivara.jdt12.miniproject.repo.DriverRepository;
import id.co.indivara.jdt12.miniproject.repo.RentCarRepository;
import id.co.indivara.jdt12.miniproject.repo.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class RentCarService extends GenericService<RentCar>{

    private final RentCarRepository rentCarRepository;
    private final DriverService driverService;
    private final CustomerService customerService;
    private final VehicleService vehicleService;

    @Autowired
    public RentCarService(RentCarRepository rentCarRepository, DriverService driverService, CustomerService customerService, VehicleService vehicleService) {
        this.rentCarRepository = rentCarRepository;
        this.driverService = driverService;
        this.customerService = customerService;
        this.vehicleService = vehicleService;
    }

    public RentCar saveWithoutDriver(RentCarRequest entity) {
        Transaction transaction = new Transaction();
        RentCar rentCar = new RentCar();
        if(entity.getDriverId() != null) {
            Driver driver = driverService.findById(entity.getDriverId());
            if(!driver.getIsAvailable()) throw new CheckAvailableVehicleAndDriverException("Data Driver dengan nama " + driver.getName() + " sedang disewa");
            driver.setIsAvailable(false);
            rentCar.setDriver(driver);
        };
        System.out.println(entity);
        Customer customer = customerService.findById(entity.getCustomerId());
        Vehicle vehicle = vehicleService.findById(entity.getVehicleId());
        if(!vehicle.getIsAvailable()) throw new CheckAvailableVehicleAndDriverException("Data Mobil dengan nama " + vehicle.getName() + " sedang disewa");
        vehicle.setIsAvailable(false);
        transaction.setStartRent(new Date());
        rentCar.setIsCurrentlyRented(true);
        rentCar.setVehicle(vehicle);
        rentCar.setCustomer(customer);
        transaction.setRentCar(rentCar);
        transaction.setIsAlreadyPaid(false);
        rentCar.setTransaction(transaction);
        return rentCarRepository.save(rentCar);
    }
    public RentCar saveWithDriver(RentCarRequest request){
        Transaction transaction =new Transaction();
        RentCar rentCar = new RentCar();
        Customer customer = customerService.findById(request.getCustomerId());
        Vehicle vehicle = vehicleService.findById(request.getVehicleId());
        if(!vehicle.getIsAvailable()) throw new CheckAvailableVehicleAndDriverException("Data Mobil dengan nama " + vehicle.getName() + " sedang disewa");
        transaction.setStartRent(new Date());
        Driver driver = driverService.getDriverByLevelandLowCount(vehicle.getLevel());
        vehicle.setIsAvailable(false);
        driver.setIsAvailable(false);
        rentCar.setIsCurrentlyRented(true);
        rentCar.setDriver(driver);
        rentCar.setCustomer(customer);
        rentCar.setVehicle(vehicle);
        transaction.setRentCar(rentCar);
        transaction.setIsAlreadyPaid(false);
        rentCar.setTransaction(transaction);
        return rentCarRepository.save(rentCar);
    }
    public List<RentCar> getAllDataDriverById(Long id){
        List<RentCar> rentCarList = new ArrayList<>();
        for(RentCar r: getAll()){
            if(r.getDriver() != null) rentCarList.add(r);
        }
        return rentCarList.stream().filter(rentCar -> Objects.equals(rentCar.getDriver().getId(), id)).collect(Collectors.toList());
    }
    public List<RentCar> getAllDataCustomerById(Long id){
        return this.getAll().stream().filter(rentCar -> Objects.equals(rentCar.getCustomer().getId(), id)).collect(Collectors.toList());
    }

    @Override
    public void delete(Long id) {
        RentCar rentCar = this.findById(id);
        Vehicle vehicle = rentCar.getVehicle();
        if(rentCar.getDriver() != null) {
            Driver driver = rentCar.getDriver();
            driver.setIsAvailable(true);
            driverService.update(driver.getId(), driver);
        }
        vehicle.setIsAvailable(true);
        vehicleService.update(vehicle.getId(), vehicle);
        super.delete(id);
    }
}
