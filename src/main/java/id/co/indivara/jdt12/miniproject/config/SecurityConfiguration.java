package id.co.indivara.jdt12.miniproject.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableMethodSecurity
public class SecurityConfiguration {
    private final JwtAuthenticationFilter jwtAuthenticationFilter;
    private final AuthenticationProvider authenticationProvider;
    private final LogoutHandler logoutHandler;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception{
        http
                .csrf()
                .disable()
                .authorizeHttpRequests()

                // path untuk swagger
                .antMatchers("/api/auth/**").permitAll()
                .antMatchers("/v2/**").permitAll()
                .antMatchers("/webjars/**").permitAll()
                .antMatchers("/api/v1/auth/**").permitAll()
                .antMatchers("/v2/api-docs").permitAll()
                .antMatchers("/v3/api-docs").permitAll()
                .antMatchers("/v3/api-docs/**").permitAll()
                .antMatchers("/swagger-resources").permitAll()
                .antMatchers("/swagger-resources/**").permitAll()
                .antMatchers("/configuration/ui").permitAll()
                .antMatchers("/configuration/security").permitAll()
                .antMatchers("/swagger-ui/**").permitAll()
                .antMatchers("/swagger-ui.html").permitAll()
                .antMatchers("/webjars/**").permitAll()

                //Role Public (tanpa login bisa mengakses)
                .antMatchers(HttpMethod.GET,"/api/vehicle/available").permitAll()
                .antMatchers(HttpMethod.GET,"/api/vehicle/{id}").permitAll()
                .antMatchers(HttpMethod.GET,"/api/transaction/payment/{id}").permitAll()

                //Role Customer
                .antMatchers(HttpMethod.GET,"/api/customer/{id}").hasAnyAuthority("CUSTOMER","ADMIN")
                .antMatchers(HttpMethod.PUT,"/api/customer/{id}").hasAnyAuthority("CUSTOMER","ADMIN")
                .antMatchers(HttpMethod.GET,"/api/rentcar/customer/**").hasAnyAuthority("CUSTOMER","ADMIN")
                .antMatchers(HttpMethod.POST,"/api/rentcar/**").hasAnyAuthority("CUSTOMER","ADMIN")
                .antMatchers(HttpMethod.GET,"/api/transaction/{id}/rent").hasAnyAuthority("CUSTOMER","ADMIN")
                .antMatchers(HttpMethod.GET,"/api/transaction/customer/**").hasAnyAuthority("CUSTOMER","ADMIN")

                //ROlE DRIVER
                .antMatchers(HttpMethod.GET,"/api/driver/{id}").hasAnyAuthority("DRIVER","ADMIN")
                .antMatchers(HttpMethod.PUT,"/api/driver/{id}").hasAnyAuthority("DRIVER","ADMIN")
                .antMatchers(HttpMethod.GET,"/api/rentcar/driver/**").hasAnyAuthority("DRIVER","ADMIN")
                .antMatchers(HttpMethod.GET,"/api/transaction/driver/**").hasAnyAuthority("DRIVER","ADMIN")

                //Role Gabungan
                .antMatchers(HttpMethod.GET,"/api/transaction/{id}").hasAnyAuthority("CUSTOMER","DRIVER","ADMIN")
                .antMatchers(HttpMethod.GET,"/api/rentcar/{id}").hasAnyAuthority("CUSTOMER","DRIVER","ADMIN")

                //Role Admin
                .antMatchers("/api/customer/**").hasAuthority("ADMIN")
                .antMatchers("/api/driver/**").hasAuthority("ADMIN")
                .antMatchers("/api/vehicle/**").hasAuthority("ADMIN")
                .antMatchers("/api/rentcar/**").hasAuthority("ADMIN")
                .antMatchers("/api/transaction/**").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.GET,"/api/auth/approval").hasAuthority("ADMIN")
                .antMatchers("/api/user/**").hasAuthority("ADMIN")




                .anyRequest()
                .authenticated()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authenticationProvider(authenticationProvider)
                .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
                .logout()
                .logoutUrl("/api/auth/logout")
                .addLogoutHandler(logoutHandler)
                .logoutSuccessHandler((request, response,authentication) -> SecurityContextHolder.clearContext());
        return http.build();

    }
}
