package id.co.indivara.jdt12.miniproject.handler;

import id.co.indivara.jdt12.miniproject.error.CheckAvailableVehicleAndDriverException;
import id.co.indivara.jdt12.miniproject.error.FinishingTheRentException;
import id.co.indivara.jdt12.miniproject.error.GetDriverByLevelAndLowestCountException;
import id.co.indivara.jdt12.miniproject.utilize.response.ErrorResponse;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.authentication.*;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedCredentialsNotFoundException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

@RestControllerAdvice
public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<ErrorResponse> notFoundDataById(NoSuchElementException ex){
        return new ResponseEntity<>(new ErrorResponse(HttpStatus.NOT_FOUND, ex.getMessage(), HttpStatus.NOT_FOUND.getReasonPhrase()),HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(FinishingTheRentException.class)
    public ResponseEntity<ErrorResponse> finishingTheRent(FinishingTheRentException ex){
        return new ResponseEntity<>(new ErrorResponse(HttpStatus.FORBIDDEN, ex.getMessage(), HttpStatus.FORBIDDEN.getReasonPhrase()),HttpStatus.FORBIDDEN);
    }
    @ExceptionHandler(CheckAvailableVehicleAndDriverException.class)
    public ResponseEntity<ErrorResponse> checkAvailableVehicleAndDriver(CheckAvailableVehicleAndDriverException ex){
        return new ResponseEntity<>(new ErrorResponse(HttpStatus.CONFLICT, ex.getMessage(), HttpStatus.CONFLICT.getReasonPhrase()),HttpStatus.CONFLICT);
    }
    @ExceptionHandler(GetDriverByLevelAndLowestCountException.class)
    public ResponseEntity<ErrorResponse> getDriverByLevelAndLowestCount(GetDriverByLevelAndLowestCountException ex){
        return new ResponseEntity<>(new ErrorResponse(HttpStatus.NOT_FOUND, ex.getMessage(), HttpStatus.NOT_FOUND.getReasonPhrase()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({SQLException.class})
    public ResponseEntity<ErrorResponse> sqlException(SQLException ex  ){
        return new ResponseEntity<>(new ErrorResponse(HttpStatus.CONFLICT, ex.getMessage() ,HttpStatus.CONFLICT.getReasonPhrase()) ,HttpStatus.CONFLICT);
    }
    @ExceptionHandler({ConstraintViolationException.class })
    public ResponseEntity<Object> handleConstraintViolation(
            ConstraintViolationException ex, WebRequest request) {
        List<String> errors = new ArrayList<String>();
        StringBuilder message = new StringBuilder();
        for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
            errors.add(violation.getRootBeanClass().getName() + " " +
                    violation.getPropertyPath() + ": " + violation.getMessage());
            message.append(violation.getMessage()).append(",");
        }

        ErrorResponse apiError =
                new ErrorResponse(HttpStatus.BAD_REQUEST, message.toString(), errors);
        return new ResponseEntity<Object>(
                apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<ErrorResponse> accessDeniedHandler(BadCredentialsException ex){
        return  new ResponseEntity<>(new ErrorResponse(HttpStatus.FORBIDDEN,ex.getMessage(), "Username atau password salah"),HttpStatus.FORBIDDEN);
    }

//    @ExceptionHandler(NullPointerException.class)
//    public ResponseEntity<ErrorResponse> nullPointerHandler(NullPointerException ex){
//        return  new ResponseEntity<>(new ErrorResponse(HttpStatus.FORBIDDEN,ex.getMessage(), "Ada error null"),HttpStatus.FORBIDDEN);
//    }
//    @ExceptionHandler(RuntimeException.class)
//    public ResponseEntity<ErrorResponse> httpClientError(RuntimeException ex){
//        return  new ResponseEntity<>(new ErrorResponse(HttpStatus.FORBIDDEN,HttpStatus.FORBIDDEN.getReasonPhrase(),ex.getMessage()),HttpStatus.FORBIDDEN);
//    }

}
