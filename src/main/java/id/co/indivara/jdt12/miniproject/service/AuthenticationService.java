package id.co.indivara.jdt12.miniproject.service;

import id.co.indivara.jdt12.miniproject.dto.request.AuthenticateRequest;
import id.co.indivara.jdt12.miniproject.dto.request.RegisterWithDriverRole;
import id.co.indivara.jdt12.miniproject.dto.request.RegisterWithRole;
import id.co.indivara.jdt12.miniproject.dto.response.AuthenticationResponse;
import id.co.indivara.jdt12.miniproject.entity.*;
import id.co.indivara.jdt12.miniproject.repo.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final UserRepository userRepository;
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    public AuthenticationResponse registerWithCustomerRole(RegisterWithRole request) {
        User user = User.builder()
                .username(request.getUsername())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(Role.CUSTOMER)
                .isEnabled(true)
                .isAccountLocked(false)
                .build();
        String jwtToken = jwtService.generateToken(user);
        Customer customer = Customer
                .builder()
                .name(request.getFullName())
                .email(request.getEmail())
                .phoneNumber(request.getPhoneNumber())
                .gender(request.getGender())
                .build();
        user.setCustomer(customer);
        user.setToken(jwtToken);
        customer.setUser(user);
        userRepository.save(user);
        return AuthenticationResponse.builder()
                .accessToken(jwtToken)
                .build();
    }
    public AuthenticationResponse registerWithAdminRole(RegisterWithRole request) {
        User user = User.builder()
                .username(request.getUsername())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(Role.ADMIN)
                .isEnabled(true)
                .isAccountLocked(false)
                .build();
        String jwtToken = jwtService.generateToken(user);
        Admin admin = Admin
                .builder()
                .name(request.getFullName())
                .email(request.getEmail())
                .phoneNumber(request.getPhoneNumber())
                .gender(request.getGender())
                .build();
        user.setAdmin(admin);
        user.setToken(jwtToken);
        admin.setUser(user);
        userRepository.save(user);
        return AuthenticationResponse.builder()
                .accessToken(jwtToken)
                .build();
    }
    public AuthenticationResponse registerWithDriverRole(RegisterWithDriverRole request) {
        User user = User.builder()
                .username(request.getUsername())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(Role.DRIVER)
                .isEnabled(false)
                .isAccountLocked(false)
                .build();
        String jwtToken = jwtService.generateToken(user);
        Driver driver = Driver
                .builder()
                .name(request.getFullName())
                .email(request.getEmail())
                .phoneNumber(request.getPhoneNumber())
                .licenceDriver(request.getLicenceDriver())
                .isAvailable(false)
                .level(3)
                .gender(request.getGender())
                .build();
        user.setDriver(driver);
        user.setToken(jwtToken);
        driver.setUser(user);
        userRepository.save(user);
        return AuthenticationResponse.builder()
                .accessToken(jwtToken)
                .build();
    }

    public AuthenticationResponse authenticate(AuthenticateRequest authenticateRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        authenticateRequest.getUsername(),
                        authenticateRequest.getPassword()
                )
        );
        User user = userRepository.findByUsername(authenticateRequest.getUsername()).orElseThrow(() -> new UsernameNotFoundException("User Not Found"));
        String jwtToken = jwtService.generateToken(user);
        user.setToken(jwtToken);
        userRepository.save(user);
        return AuthenticationResponse.builder()
                .accessToken(jwtToken)
                .build();
    }
    public User findByUsername(String username){return userRepository.findByUsername(username).orElseThrow(()->new UsernameNotFoundException("User Not Found"));}
    public User approvalDriver(Long id){
        return userService.adminApproval(id);
    }
//    @Bean
//    public void addAdmin(){
//        this.registerWithAdminRole(
//                new RegisterWithRole("admin","admin","admin admin","admin@gmail.com","08127312731",Gender.Male)
//        );
//    }
}
